Mesa 19.2.5 Release Notes / 2019-11-20
======================================

Mesa 19.2.5 is a bug fix release which fixes bugs found since the 19.2.4
release.

Mesa 19.2.5 implements the OpenGL 4.5 API, but the version reported by
glGetString(GL_VERSION) or glGetIntegerv(GL_MAJOR_VERSION) /
glGetIntegerv(GL_MINOR_VERSION) depends on the particular driver being
used. Some drivers don't support all the features required in OpenGL
4.5. OpenGL 4.5 is **only** available if requested at context creation.
Compatibility contexts may report a lower version depending on each
driver.

Mesa 19.2.5 implements the Vulkan 1.1 API, but the version reported by
the apiVersion property of the VkPhysicalDeviceProperties struct depends
on the particular driver being used.

SHA256 checksum
---------------

::

       3d010a366b28d10bdd71e32091d8684baf1522e6466c5c5703667091b2108c8b  mesa-19.2.5.tar.xz

New features
------------

-  None

Bug fixes
---------

-  HSW. Tropico 6 and SuperTuxKart have shadows flickering
-  glxgears segfaults on POWER / Xvnc
-  Cannot start Civ6 with AMD GPU on Linux

Changes
-------

-  llvmpipe: use ppc64le/ppc64 Large code model for JIT-compiled shaders
-  Call shmget() with permission 0600 instead of 0777
-  spirv: Don't leak GS initialization to other stages
-  i965: Unify CC_STATE and BLEND_STATE atoms on Haswell as a workaround
-  docs: Add SHA256 sum for for 19.2.4
-  cherry-ignore: Update for 19.2.4 cycle
-  egl: fix \_EGL_NATIVE_PLATFORM fallback
-  nir/algebraic: Add the ability to mark a replacement as exact
-  nir/algebraic: Mark other comparison exact when removing a == a
-  mesa/main: Ignore filter state for MS texture completeness
-  anv: Stop bounds-checking pushed UBOs
-  gallium: dri2: Use index as plane number.
-  anv: invalidate file descriptor of semaphore sync fd at vkQueueSubmit
-  anv: remove list items on batch fini
-  anv/wsi: signal the semaphore in the acquireNextImage
-  st/mesa: fix Sanctuary and Tropics by disabling ARB_gpu_shader5 for
   them
-  tgsi_to_nir: fix masked out image loads
-  tgsi_to_nir: handle PIPE_FORMAT_NONE in image opcodes
-  intel/compiler: fix nir_op_{i,u}*32 on ICL
-  radeonsi: disable sdma for gfx10
-  radeonsi: tell the shader disk cache what IR is used
-  radeonsi: fix shader disk cache key
