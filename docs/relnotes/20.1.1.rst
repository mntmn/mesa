Mesa 20.1.1 Release Notes / 2020-06-10
======================================

Mesa 20.1.1 is a bug fix release which fixes bugs found since the 20.1.0
release.

Mesa 20.1.1 implements the OpenGL 4.6 API, but the version reported by
glGetString(GL_VERSION) or glGetIntegerv(GL_MAJOR_VERSION) /
glGetIntegerv(GL_MINOR_VERSION) depends on the particular driver being
used. Some drivers don't support all the features required in OpenGL
4.6. OpenGL 4.6 is **only** available if requested at context creation.
Compatibility contexts may report a lower version depending on each
driver.

Mesa 20.1.1 implements the Vulkan 1.2 API, but the version reported by
the apiVersion property of the VkPhysicalDeviceProperties struct depends
on the particular driver being used.

SHA256 checksum
---------------

::

   3ea6e46ea7881c656f7b4724639eaa4672d4e0e0b70869651e8f955ebae3d476  mesa-20.1.1.tar.xz

New features
------------

-  None

Bug fixes
---------

-  i965: Rendering problems replaying a trace of "Refunct" after
   mesa-20.1.0-rc1 release [bisected]
-  gallium/winsys/radeon/drm fails assertion on 32bit
-  NIR validation failed after glsl to nir, before function inline,
   wrong {src,dst}->type ?
-  Mesa 20.0.7 / 20.1.0-rc4 regression, extremally long shader
   compilation time in NIR
-  Mesa-git build fails on Fedora Rawhide
-  Doom Eternal 1.1 performs very poorly on RADV
-  iris/i965: possible regression in 20.0.5 due to changes in buffer
   manager sharing across screens (firefox/mozilla#1634213)
-  iris/i965: possible regression in 20.0.5 due to changes in buffer
   manager sharing across screens (firefox/mozilla#1634213)
-  Incorrect \_NetBSD_\_ macro inside execmem.c
-  Possible invalid sizeof in device.c
-  GLSL compiler assertion is_float() failed in glsl/ir_validate.cpp,
   visit_leave on specific WebGL shader
-  [RADV] - Doom Eternal (782330) & Metro Exodus (412020) - Title
   requires 'RADV_DEBUG=zerovram' to eliminate colorful graphical
   aberrations.
-  [RADV] - Doom Eternal (782330) & Metro Exodus (412020) - Title
   requires 'RADV_DEBUG=zerovram' to eliminate colorful graphical
   aberrations.
-  mesa trunk master vulkan overlay-layer meson.build warning empty
   configuration_data() object

Changes
-------

-  pan/bi: Fix emit_if successor assignment
-  glsl: fix crash on glsl macro redefinition
-  llvmpipe: do not enable tessellation shader without llvm coroutines
   support
-  radv: Always expose non-visible local memory type on dedicated GPUs
-  glsl: Don't replace lrp pattern with lrp if arguments are not floats
-  glsl: inline functions with unsupported return type before converting
   to nir
-  i965: Work around incorrect usage of glDrawRangeElements in UE4
-  llvmpipe: move coroutines out of noopt case
-  vulkan-overlay/meson: use install_data instead of configure_file
-  docs/relnotes add sha256 sums to 20.1.0
-  docs: drop new_features.txt
-  .pick_status.json: Update to 3a1a40b4431d505fa6487cd012ddb4b64387aee5
-  glapi: remove deprecated .getchildren() that has been replace with an
   iterator
-  intel: fix gen_sort_tags.py
-  zink: Use store_dest_raw instead of storing an uint
-  nir: reuse existing psiz-variable
-  nir: lower_tex: Don't normalize coordinates for TXF with RECT
-  nouveau: allow invalidating coherent/persistent buffer backings
-  intel/vec4: Stomp the return type of RESINFO to UINT32
-  intel/fs: Fix unused texture coordinate zeroing on Gen4-5
-  freedreno/a6xx: use nonbinning VS when GS is used
-  iris: fix BO destruction in error path
-  i965: don't forget to set screen on duped image
-  i965: fix export of GEM handles
-  iris: fix export of GEM handles
-  radeonsi: add a hack to disable TRUNC_COORD for shadow samplers
-  util: Initialize pipe_shader_state for passthrough and transform
   shaders
-  vc4_bufmgr: fix time_t printf
-  pan_bo.h: add time.h include for time_t
-  v3d_bufmgr: fix time_t printf
-  winsys/radeon: do not cast bo->va as void\*
-  ac/surface: set SCANOUT if surf->is_displayable
-  ac/surface: fix epitch when modifying surf_pitch
-  aco: fix interaction with 3f branch workaround and p_constaddr
-  aco: consider SDWA during value numbering
-  aco: check instruction format before waiting for a previous SMEM
   store
-  aco: preserve more fields when combining additions into SMEM
-  freedreno/computerator: fix missing dependency on generated header
-  spirv,radv,anv: implement no-op VK_GOOGLE_user_type
-  aco: fix register allocation for subdword instructions on GFX10
-  radv: enable zero VRAM for Doom Eternal
-  radv: enable zero VRAM for all VKD3D (DX12->VK) games
-  nir/lower_explicit_io: fix NON_UNIFORM access for UBO loads
-  intel/dev: Don't consider all TGL SKUs as GT1 only
-  radv: fix regression with builtin cache
-  glsl: fix potential slow compile times for GLSLOptimizeConservatively
-  pan/bi: Initialize struct fma_op_info member extended.
-  zink: Check fopen result.
-  etnaviv: Fix memory leak on error path.
-  r300g: Remove extra printf format specifiers.
-  vdpau: Fix wrong calloc sizeof argument.
-  mesa: Fix NetBSD compiler macro.
-  intel/genxml: Migrate from deprecated xml.etree.ElementTree
   getchildren.
-  Switch from cElementTree to ElementTree.
